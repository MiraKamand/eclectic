import React, { Component } from 'react';
import logo from './logo.svg';
import { Redirect } from 'react-router-dom'
import './styles.less'
import ImgSlider from './Components/ImgSlider'
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Progress,
  Input,
  Row,
  Col,
} from "reactstrap";
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      sudokuModal: false
    }
    this.toggle = this.toggle.bind(this);
    setTimeout(() => {
      this.setState({
        sudokuModal: true
      })
    },1000)
  }
  toggle() {
    this.setState(prevState => ({
      sudokuModal: !prevState.sudokuModal
    }));
  }
  render() {
    return (
      <div className="App">
        <div className="menu">
          <a href="#title-header" className="anchor-eclectic"><div className="menu-tab" id='home' href="#home"> <p id="home-p">Home</p> </div></a>
          <a href="#aboutus" className="anchor-eclectic"><div className="menu-tab" id='about'> About Us</div></a>
          <a href="#contactus" className="anchor-eclectic"><div className="menu-tab" id='contact'>Contact Us</div></a>
        </div>
        <div className='stripes-shadow'>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 474.07"><defs>
            <style>
            </style></defs><title>Asset 2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1-shadow" d="M22.5,471.5l-21-52L.5.5h22Z" /><path class="cls-2-shadow" d="M64.5,471.5l-31-53L32.5.5h32Z" /><path class="cls-3-shadow" d="M99.5,432.5l-25,39V.5h25Z" /><path class="cls-4-shadow" d="M160.5,445.5" /></g></g></svg>
        </div>
        <div className='stripes'>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 474.07"><defs>
            <style>
            </style></defs><title>Asset 2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M22.5,471.5l-21-52L.5.5h22Z" /><path class="cls-2" d="M64.5,471.5l-31-53L32.5.5h32Z" /><path class="cls-3" d="M99.5,432.5l-25,39V.5h25Z" /><path class="cls-4" d="M160.5,445.5" /></g></g></svg>
        </div>

        <section id='title-header'>
          <h1 id="eclectic-title" className="h1-glow" > Eclectic</h1>
          <h3 id='sub-title' className="h1-glow-2">Your Life . . . STYLE </h3>
        </section>


      
        <section  >
          <div className="grid960">
            <Row>
              <Col className="h1-glow-2" id="aboutus">
                <h1 ><i>About Eclectic</i></h1>
              </Col>
            </Row>
            <Row>
              <Col id="about_section" >
                <h4 className="h1-glow-2">Eclectic means a style from a broad and diverse range of sources.
                We provide a non-cliché styling taste to any occasion you have!
               Check our styling albums and benefit form a 10% discount on you first order!</h4>
              </Col>
            </Row>
          </div>
        </section>

        <section id='img-slider'>
          <ImgSlider history={this.props.history}></ImgSlider>
        </section>

        <section id='meet-the-team'>
          <div className="grid960">
            <Row className="meetus-title">
              {/* <h3>Meet the Eclectic Team</h3> */}
            </Row>
            <Row>
              <Col sm={4}>
                <Card className="meetus-card">
                  <CardHeader className="empty-space">

                  </CardHeader>
                  <CardBody className="meetus-card-body">
                    WE ARE THE FASHION VISIONARIES
                  </CardBody>
                </Card>
              </Col>
              <Col sm={4}>
                <Card id="Mira">
                  <CardBody>
                    <div ></div>
                  </CardBody>
                  <CardFooter className="meetus-card-footer">
                    Mira Al Kamand
                  </CardFooter>
                </Card>
              </Col>
              <Col sm={4}>
                <Card id="Bahaa">
                  <CardBody>
                    <div ></div>
                  </CardBody>
                  <CardFooter className="meetus-card-footer">
                    Bahaa Shaaban
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </div>
        </section>
        <section id="contactus" className="h1-glow-2" >
          <div className="grid960">
            <h2 id='contact-us-title'> <i>Contact Us </i></h2>
            <form className="contact-us-form" method="post" action='http://localhost/contactus.php'>
              <Row>
                <label >E-mail:</label>
                <input name="email" type='email' required className="input-form" placeholder="Enter your email here"></input>
              </Row>
              <Row>
                <label>Message:</label>
                <textarea name="message" type='text' required placeholder="Enter message/question here"></textarea>
              </Row>
              <Row>
                <label>Phone Number:</label>
                <input name="number" type='number' required ></input>
              </Row>
              <Row>
                <input type="submit" id="submit-button"></input>
              </Row>
            </form>
          </div>
        </section>

        <section id='page-footer' >
          <div className='grid960'>
            <Row>
              <Col sm={2} className='footer-list'>
                <Row className="footer-title">
                  Eclectic
                </Row>
                <Row>
                  About us
                </Row>
                <Row>
                  Contact us
                </Row>
                <Row>
                  Want to collaborate
                </Row>
                <Row>
                  Become a stylist
                </Row>
              </Col>
              <Col sm='2' className='footer-list'>
                <Row className="footer-title">
                  Eclectic
                </Row>
                <Row>
                  Shop our styles
                </Row>
                <Row>
                 Leave a comment
                </Row>
                <Row>
                 Sign Up
                </Row>
                <Row>
                  Follow our pages
                </Row>
              </Col>
              <Col sm='6' className='footer-list'>
                <Row className="footer-title">
                  Follow Us
                </Row>
                <Row className='social-icons'>
                  <i className="fab fa-facebook"></i>
                  <i class="fab fa-instagram"></i>
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-snapchat-ghost"></i>
                  <i class="fab fa-youtube"></i>
                </Row>
                <Row id="signup-text">
                  Be the first to know about the latest deals, secret sales, style updates & more!
                </Row>
                <form method="post" action='http://localhost/newsletter.php'>
                  <input type="email" name="email" className='signup-input' placeholder='Email..'></input>
                  <input type="submit" id="signup-button" value="SIGN UP"></input>
                </form>
              </Col>
            </Row>
          </div>
        </section>
        <Modal isOpen={this.state.sudokuModal} toggle={this.toggle} className='modal-sudoku'>
        <ModalHeader toggle={this.toggle}>Fun  and games</ModalHeader>
        <ModalBody>
            While you browse eclectic play a game of sudoku
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => {
              console.log('redirectin')
              window.location = "http://localhost:3001"
            }}>Play</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>

    );

  }
}

export default App;
