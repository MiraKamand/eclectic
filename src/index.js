import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Techni from './pages/techni';
import Classy from './pages/classy';
import Tropical from './pages/tropical';
import * as serviceWorker from './serviceWorker';
import ThankYou from './pages/thankyou';
import {  BrowserRouter as Router, Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

ReactDOM.render(
    <Router>
    <Route exact path='/' component={App}/>
    <Route exact path='/tropical' component={Tropical}/>
    <Route exact path='/classy' component={Classy}/>
    <Route exact path='/techni' component={Techni}/>
    <Route exact path='/thankyou' component={ThankYou}/>
    </Router>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
