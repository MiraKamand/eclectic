import React, { Component } from 'react';
import '../App.css';
import {
  Card,
  CardBody,
  CardFooter,
  Label,
  Row,
  Col,
} from "reactstrap";
const jsonData = [
  {
    url: 'Images/7.jpg',
    footer: 'Jumpsuit',
    price: '49 USD'
  },
  {
    url: 'Images/8.jpg',
    footer: 'Dress',
    price: '60 USD'
  },
  {
    url: 'Images/9.jpg',
    footer: 'Dress',
    price: '76 USD'
  },
  {
    url: 'Images/10.jpg',
    footer: 'Jumpsuit',
    price: '40 USD'
  },
  {
    url: 'Images/11.jpg',
    footer: 'Dress',
    price: '85 USD'
  },
  {
    url: 'Images/12.jpg',
    footer: 'Selopette',
    price: '39 USD'
  }
]
class Techni extends Component {
  render() {
    return (
      <div className="App">
        <div className="menu">
          <a href="/" className="anchor-eclectic"><div className="menu-tab" id='home' href="/"> <p id="home-p">Back</p> </div></a>
        </div>
        <div className='stripes-shadow'>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 474.07"><defs>
            <style>
            </style></defs><title>Asset 2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1-shadow" d="M22.5,471.5l-21-52L.5.5h22Z" /><path class="cls-2-shadow" d="M64.5,471.5l-31-53L32.5.5h32Z" /><path class="cls-3-shadow" d="M99.5,432.5l-25,39V.5h25Z" /><path class="cls-4-shadow" d="M160.5,445.5" /></g></g></svg>
        </div>
        <div className='stripes'>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 474.07"><defs>
            <style>
            </style></defs><title>Asset 2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M22.5,471.5l-21-52L.5.5h22Z" /><path class="cls-2" d="M64.5,471.5l-31-53L32.5.5h32Z" /><path class="cls-3" d="M99.5,432.5l-25,39V.5h25Z" /><path class="cls-4" d="M160.5,445.5" /></g></g></svg>
        </div>
        <section id='shop-page-header'>
          <h1 id="shop-page-title" className="h1-glow-2" >The Tropical Look</h1>
        </section>
        {/* <section id='title-header'>

                </section> */}



        <section id="shop-page-subheader">
          <div className="grid960">
            <Row>
              <Col id="aboutus" className="h1-glow-2">
                <h1><i>Checkout our newest collection</i></h1>
              </Col>
            </Row>
            <Row>
              <Col>
                <h4>Get the latest trends 50% off today</h4>
              </Col>
            </Row>
          </div>
        </section>

        <section id="shop-menu">
          <div className="grid960">
            <Row className="shop-menu-cart">
              <Col>
                <i class="fas fa-star"></i>
                Wishlist
                        </Col>
              <Col>
                <i class="fas fa-shopping-cart"></i>
                Shopping Cart
                        </Col>
              <Col>
                <i class="fas fa-check-square"></i>
                Checkout
                        </Col>
            </Row>
          </div>
        </section>

        <div>
          <div className="grid960">
            <Row>
              {jsonData.map(element => {
                return <Col>
                  <Card className="shop-element" style={{ backgroundImage: `url('${element.url}')` }}>
                    <CardBody>
                      <div ></div>
                    </CardBody>
                    <CardFooter className="shop-card-footer">
                      <Row>
                        <Col>
                          Style:{element.footer}
                        </Col>
                        <Col>
                          {element.price}
                        </Col>

                      </Row>
                      <Row>
                        <Col sm={4}>
                        </Col>
                        <Col id="add-cart">
                          Add to Cart <i class="fas fa-cart-plus"></i>
                        </Col>
                      </Row>
                    </CardFooter>
                  </Card>
                </Col>
              })}
            </Row>
          </div>
        </div>

        <section id="shop-menu">
          <div className="grid960">

          </div>
        </section>


        <section id='page-footer' >
          <div className='grid960'>
            <Row>
              <Col sm={2} className='footer-list'>
                <Row className="footer-title">
                  Eclectic
                </Row>
                <Row>
                  About us
                </Row>
                <Row>
                  Contact us
                </Row>
                <Row>
                  Want to collaborate
                </Row>
                <Row>
                  Become a stylist
                </Row>
              </Col>
              <Col sm='2' className='footer-list'>
                <Row className="footer-title">
                  Eclectic
                </Row>
                <Row>
                  Shop our styles
                </Row>
                <Row>
                 Leave a comment
                </Row>
                <Row>
                 Sign Up
                </Row>
                <Row>
                  Follow our pages
                </Row>
              </Col>
              <Col sm='6' className='footer-list'>
                <Row className="footer-title">
                  Follow Us
                </Row>
                <Row className='social-icons'>
                  <i className="fab fa-facebook"></i>
                  <i class="fab fa-instagram"></i>
                  <i class="fab fa-twitter"></i>
                  <i class="fab fa-pinterest-p"></i>
                  <i class="fab fa-snapchat-ghost"></i>
                  <i class="fab fa-youtube"></i>
                </Row>
                <Row id="signup-text">
                  Be the first to know about the latest deals, secret sales, style updates & more!
                </Row>
                <form method="post" action='http://localhost/newsletter.php'>
                  <input type="email" name="email" className='signup-input' placeholder='Email..'></input>
                  <input type="submit" id="signup-button" value="SIGN UP"></input>
                </form>
              </Col>
            </Row>
          </div>
        </section>
      </div>
    )
  }
}

export default Techni;