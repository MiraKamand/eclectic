import React, { Component } from 'react';
import '../App.css';
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    Modal,
    Container,
    CardFooter,
    CardTitle,
    Label,
    FormGroup,
    Progress,
    Input,
    Row,
    Col,
} from "reactstrap";
class Techni extends Component {
    // constructor(props) {
    //     super(props)

    // }
    // componentDidMount() {
    //     fetch('http://localhost/contactus.php')
    //         .then(function (response) {
    //             console.log(response.body)
    //             return response;
    //         })
    // }
    render() {
        return (
            <div className="App">
                <div className="menu">
                    <a href="/" className="anchor-eclectic"><div className="menu-tab" id='home' href="/"> <p id="home-p">Back</p> </div></a>
                </div>
                <div className='stripes-shadow'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 474.07"><defs>
                        <style>
                        </style></defs><title>Asset 2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1-shadow" d="M22.5,471.5l-21-52L.5.5h22Z" /><path class="cls-2-shadow" d="M64.5,471.5l-31-53L32.5.5h32Z" /><path class="cls-3-shadow" d="M99.5,432.5l-25,39V.5h25Z" /><path class="cls-4-shadow" d="M160.5,445.5" /></g></g></svg>
                </div>
                <div className='stripes'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 474.07"><defs>
                        <style>
                        </style></defs><title>Asset 2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M22.5,471.5l-21-52L.5.5h22Z" /><path class="cls-2" d="M64.5,471.5l-31-53L32.5.5h32Z" /><path class="cls-3" d="M99.5,432.5l-25,39V.5h25Z" /><path class="cls-4" d="M160.5,445.5" /></g></g></svg>
                </div>
                <section id='title-header'>
                    <h1 id="shop-page-title" className="" >Thank You for submitting</h1>
                </section>
            </div>
        )
    }
}

export default Techni;