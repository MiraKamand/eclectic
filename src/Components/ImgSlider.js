import React, { Component } from 'react';
import Coverflow from 'react-coverflow';


class ImgSlider extends Component {
    render() {
        return(
            <Coverflow   width="2000px" height="500"
                displayQuantityOfSide={2}
                navigation={false}
                enableScroll={true}
                clickable={true}
                active={0}
                infiniteScroll={true}
            >
                {/* <div
                // onClick={() => fn()}
                // onKeyDown={() => fn()}
                role="menuitem"
                tabIndex="0"
                >
                <img
                    src='images/colorful2.jpg'
                    alt='title or description'
                    style={{
                    display: 'block',
                    width: '100%',
                    }}
                />
                </div> */}
                {/* <img src='images/summer1.jpg' alt='Summer Vibes' data-action="http://localhost:3000/tropical"/> */}
                <img src='images/summer1.jpg' alt='Summer Vibes' onClick={() => {
                    this.props.history.push('/tropical')
                }}/>
                {/* <img src='images/classy.jpg' alt='KEEP IT CLASSY' data-action="http://localhost:3000/classy"/> */}
                <img src='images/classy.jpg' alt='KEEP IT CLASSY' onClick={() => {
                    this.props.history.push('/classy')
                }}/>
                {/* <img src='images/colorful1.jpg' alt='Life in Technicolors' data-action="http://localhost:3000/techni"/> */}
                <img src='images/colorful1.jpg' alt='Life in Technicolors' onClick={() => {
                    this.props.history.push('/techni')
                }}/> 
            </Coverflow>
        );
    }
}

export default ImgSlider;


